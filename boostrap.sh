#!/bin/bash -x

PACKAGES="git ansible"
GIT_REMOTE_URL="https://gitlab.com/heyitsemily/dev-env.git"

function install_depends() {
  if type apt >/dev/null 2>/dev/null; then
    sudo apt update && sudo apt install -y $PACKAGES || exit $?
    return
  fi
  
  echo "Distribution not supported" && exit 2
}

install_depends
if [[ ! -d .git ]]; then
  git clone $GIT_REMOTE_URL ./$(basename $GIT_REMOTE_URL)
  pushd $(basename $GIT_REMOTE_URL)
fi

ansible-playbook -u $(whoami) site.yml
popd 2>/dev/null || true
