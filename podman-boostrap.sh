#!/bin/bash -x

find /etc/ -maxdepth 1 -name "*release*" | xargs --verbose -n1 -I% source %

if [[ $NAME = "Ubuntu" ]]; then
  sudo sh -c "echo 'deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/ /' > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list"
  curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_${VERSION_ID}/Release.key | sudo apt-key add -
  sudo apt-get update -qq
  sudo apt-get -qq -y install podman
fi

