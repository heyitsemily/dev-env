filetype plugin indent on    " required

" Solarized settings
set t_Co=16
syntax enable
let g:solarized_termtrans=1
let g:solarized_termcolors=16
let g:solarized_contrast = "high"
set background=dark
colorscheme solarized
" colorscheme elflord 

" start NERDTree automatically
" autocmd vimenter * NERDTree

set sw=4
set ts=4
set et
set ai
set backspace=indent,eol,start

" Enable folding
set foldmethod=indent
set foldlevel=99
" use space to toggle folding instead of 'za'
nnoremap <space> za
vnoremap <space> zf

" Text wrapping
set tw=79
set fo+=t

set hlsearch
set number

" set a 'soft' column at 80 and a 'hard' stop at 120 characters wide
let &colorcolumn="80,".join(range(120,999),",")
set cursorline
