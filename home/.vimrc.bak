" Vundle
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'
" Plugin 'davidhalter/jedi-vim'
" Plugin 'ervandew/supertab'
" Plugin 'ycm-core/YouCompleteMe'
Plugin 'altercation/vim-colors-solarized'
Plugin 'preservim/nerdtree'
Plugin 'tpope/vim-fugitive'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
" Plugin 'tpope/vim-fugitive'

" All of your Plugins must be added before the following line
call vundle#end()            " required

filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Solarized settings
set t_Co=16
syntax enable
let g:solarized_termtrans=1
let g:solarized_termcolors=16
let g:solarized_contrast = "high"
set background=dark
colorscheme solarized
" colorscheme elflord 

" let g:jedi#completions_command = "<Tab>"
let g:ycm_server_python_interpreter = '/usr/bin/python3.8'

" start NERDTree automatically
" autocmd vimenter * NERDTree

" syntax highlighting for files that start with "Dockerfile"
augroup docker_ft
  au!
  autocmd BufNewFile,BufRead Dockerfile*   set syntax=sh
augroup END

set sw=4
set ts=4
set et
set ai
set backspace=indent,eol,start

" Enable folding
set foldmethod=indent
set foldlevel=99
" use space to toggle folding instead of 'za'
nnoremap <space> za
vnoremap <space> zf

" Text wrapping
set tw=79
set fo+=t

set hlsearch
set number

" set a 'soft' column at 80 and a 'hard' stop at 120 characters wide
let &colorcolumn="80,".join(range(120,999),",")
set cursorline
